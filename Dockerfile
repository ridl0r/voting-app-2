# Hier nutzen wir als Grundlage ein python runtime alsBasis Image
FROM python:2.7

# Setzen des Applikation verzeichnisses 
WORKDIR /app

# Installieren unserer requirements.txt
ADD requirements.txt /app/requirements.txt
RUN pip install -r requirements.txt

# Kopieren des Codes aus dem aktuellen Ordner nach /app innerhalb des Containers
ADD . /app

# Port 80 freigegeben für links und/oder publish
EXPOSE 80 

# Definieren des Command das beim Start des Containers ausgeführt wird.
CMD ["python", "app.py"]
